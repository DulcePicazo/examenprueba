package com.example.saludo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.limpiar();
    }

    public void btnEnviarClick(View view) {

        String _nombre;
        int _edad;
        EditText nombre = (EditText) findViewById(R.id.nombre);
        EditText edad = (EditText) findViewById(R.id.edad);

        _nombre = nombre.getText().toString();
        _edad = Integer.valueOf(edad.getText().toString());


        Intent intento = new Intent(getApplicationContext(),Saludar.class);
        intento.putExtra("nombre",_nombre);
        intento.putExtra("edad",_edad);
        startActivity(intento);
  

    }
    public void btnSalir1Click(View view) {
        finish();
    }
    public void limpiar(){
        EditText nombre = (EditText) findViewById(R.id.nombre);
        EditText edad = (EditText) findViewById(R.id.edad);
        nombre.setText("");
        edad.setText("");
    }
}