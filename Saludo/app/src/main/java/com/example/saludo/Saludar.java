package com.example.saludo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class Saludar extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saludar);
        this.llenarSaludo();
    }

    public void llenarSaludo(){
        String nombre = getIntent().getStringExtra("nombre");
        int edad = getIntent().getIntExtra("edad",0);
        TextView textViewSaludo = (TextView) findViewById(R.id.textViewSaludo);
        textViewSaludo.setText("Hola "+nombre + " tu edad es " + edad);
    }

    public void btnSalirClick(View view) {
        finish();
    }
}